%{
 extern "C" int yylex();
 #include "parser.tab.c"  // Defines the tokens
%}

%%
[0-9]+ { 
    cout << "INT: (" << yytext << ")" << endl;
    yylval.intVal = atoi(yytext); 
    return INTEGER_LITERAL; 
}

[0-9]+.[0-9]+ {
    cout << "FLOAT: (" << yytext << ")" << endl; 
    yylval.floatVal = atof(yytext); return FLOAT_LITERAL; 
}

"+" { 
    cout << "PLUS ENCOUNTERED" << endl; 
    return PLUS; 
}

"-" {
    cout << "MINUS ENCOUNTERED" << endl;
    return MINUS; 
}

"*" {
    cout << "MULTIPLY ENCOUNTERED" << endl;
    return MULT;
}

"/" {
    cout << "DIVISION ENCOUNTERED" << endl;
    return DIV; 
}

";" {
    cout << "SEMICOLON ENCOUNTERED" << endl;
    return SEMI;
}
[ \t\r\n\f] ;


